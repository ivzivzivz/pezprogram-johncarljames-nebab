import java.util.ArrayList;
import java.io.*;

public class Loadgrid
{
	public static void main(String[] args) {
		if(args.length<1) {
			System.out.println("Provide a paramater");
			System.out.println("USAGE: java Loadgrid <string filename>");
			return;
		}
		if(args.length>=1) {
			if(args[0].equalsIgnoreCase("load") && args.length==2) {
				System.out.println(loadGrid(args[1]));
			}
			else if(args[0].equalsIgnoreCase("diss") && args.length==6) {
				System.out.println(dissimilarity(loadGrid(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[5])));
			}
		}
	}

	public static ArrayList<String> loadGrid(String filename) {
		ArrayList<String> grid = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line;
			while((line = br.readLine()) != null) {
				grid.add(line);
			}
			br.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return(grid);
	}

	public static float dissimilarity(ArrayList<String> arr, int row1, int col1, int row2, int col3) {
		int arrX = 0;
		int arrY = 0;
		float result = (float)0.0;
		for(int i=0; i<arr.size(); i++) {
			char c = arr.get(i).charAt(0);
			if(c == ('x')) {
				arrX++;
			}
			else if(c == ('o')) {
				arrY++;
			}
		}
		if(arrX != 0 && arrY != 0) {
			result = (float)(0.5 * ((row1/arrX)-(row2)));
		}
		return(result);
	}
}
